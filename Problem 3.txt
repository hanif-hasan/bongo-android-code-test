Question: Write a video player application with ‘Play’, ‘Forward’ , 
		‘Rewind’ functionalities. Please write pseudocode for this 
		program and explain the design pattern you will use to develop
		these three functionalities.


Answer:

public class VideoPlayer{
    
	private final int COUNT = 30;
	private int index=1;
	private Boolean isRewind=flase;
	private Boolean isForwared=flase;
	
	public void play(){
		VideoView videoView =(VideoView)findViewById(R.id.vdVw);
		MediaController mediaController= new MediaController(this); 
		mediaController.setAnchorView(videoView);
		Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/video" + index);
		videoView.setMediaController(mediaController);
		videoView.setVideoURI(uri);
		videoView.requestFocus();
		videoView.start();
	}
	
	public void forward(){
		isForwared=true;
		if(!isRewind){}
		if(COUNT>index){
			index++;
		}
		else{
			index=1;
		}
		play();
	}
   
	public void Rewind(){
		if(isForwared){
			index--;
        }
		isRewind=true;
		play();
		isForwared=false;
	}
	
	public void UnRewind(){
        	isRewind=false;
	}
}


I will will use MVVM design pattern with live data while developing this android app. 
As live data will keep track of video position and video list regardless of application lifecycle. 
And MVVM will keep my code more robust, moduler, easy for unit testing.